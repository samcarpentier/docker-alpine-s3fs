#!/bin/bash

if [ -z "$S3_ACCESS_KEY" ]; then
  >&2 echo "Variable S3_ACCESS_KEY not provided"
  exit 1
fi
if [ -z "$S3_SECRET_KEY" ]; then
  >&2 echo "Variable S3_SECRET_KEY not provided"
  exit 1
fi
if [ -z "$S3_URL" ]; then
  >&2 echo "Variable S3_URL not provided"
  exit 1
fi
if [ -z "$S3_BUCKET" ]; then
  >&2 echo "Variable S3_BUCKET not provided"
  exit 1
fi

if [ -z "$MOUNTPOINT" ]; then
  MOUNTPOINT="/mnt/s3fs/${BUCKET}"
fi

mkdir -p $MOUNTPOINT

echo "${S3_ACCESS_KEY}:${S3_SECRET_KEY}" > /etc/passwd-s3fs
chmod 600 /etc/passwd-s3fs

/usr/bin/s3fs ${S3_BUCKET} ${MOUNTPOINT} \
  -o allow_other \
  -o nonempty \
  -o use_path_request_style \
  -o url="${S3_URL}" \
  -o parallel_count="16"
